#!/bin/sh

TOKEN="$1"
ADDR="localhost:8080"

# set up members
curl -H "x-authentication: $TOKEN" -X "POST" -d '{"num": 900, "login": "admin", "name": "Administradora", "email": "admin@example.com", "phone": "654321123", "password": "admin", "role": "admin", "balance": 10000}' $ADDR/api/member
curl -H "x-authentication: $TOKEN" -X "POST" -d '{"num": 901, "login": "socia", "name": "Socia Aicos", "email": "socia@example.com", "phone": "678900123", "password": "socia", "role": "member", "balance": 10000}' $ADDR/api/member

# set up products
curl -H "x-authentication: $TOKEN" -X "POST" -d '{"code": 234, "name": "aceite", "price": 1700, "stock": 35}' $ADDR/api/product
curl -H "x-authentication: $TOKEN" -X "POST" -d '{"code": 120, "name": "alubias", "price": 200, "stock": 60}' $ADDR/api/product
curl -H "x-authentication: $TOKEN" -X "POST" -d '{"code": 302, "name": "esparragos", "price": 300, "stock": 45}' $ADDR/api/product
curl -H "x-authentication: $TOKEN" -X "POST" -d '{"code": 320, "name": "limpia suelos", "price": 450, "stock": 40}' $ADDR/api/product
curl -H "x-authentication: $TOKEN" -X "POST" -d '{"code": 112, "name": "arroz", "price": 120, "stock": 50}' $ADDR/api/product
