import React from "react";
import { Row, Form, Spinner, Alert } from "react-bootstrap";
import AuthContext from "./AuthContext";
import { ResponseError } from "./errors";
import { url } from "./util";

class Sender extends React.Component {
  static contextType = AuthContext;

  constructor(props) {
    super(props);
    this.state = {
      isLodaing: false,
      error: "",
    };
  }

  submit(e) {
    if (e) {
      e.preventDefault();
    }

    this.setState({ isLoading: true });
    const method = this.props.method ? this.props.method : "POST";

    const bodyObj =
      typeof this.props.body === "function"
        ? this.props.body()
        : this.props.body;
    const body = JSON.stringify(bodyObj);
    fetch(url(this.props.url), {
      headers: { "x-authentication": this.context.token },
      method,
      body,
    })
      .then((response) => {
        this.setState({ isLoading: false });
        if (!response.ok) {
          throw new ResponseError(response);
        }

        if (response.headers.get("content-type") !== "application/json") {
          return response.text();
        }
        return response.json();
      })
      .then((data) => {
        this.props.onSuccess(data);
      })
      .catch((e) => {
        if (
          e instanceof ResponseError &&
          this.props.onError &&
          this.props.onError(e.response.status)
        ) {
          return;
        }

        this.setState({ error: e.message });
      });
  }

  render() {
    if (this.state.isLoading) {
      return (
        <Row className="justify-content-center">
          <Spinner animation="border" />
        </Row>
      );
    }
    let error;
    if (this.state.error) {
      error = (
        <Alert variant="danger">
          Se produjo un error enviando datos: {this.state.error}
        </Alert>
      );
    }

    return (
      <Form onSubmit={(e) => this.submit(e)}>
        {error}
        {this.props.children}
      </Form>
    );
  }
}

export default Sender;
