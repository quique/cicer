import { createContext } from "react";

export const AuthContext = createContext({
  num: 0,
  role: "",
  token: null,
});

export default AuthContext;
