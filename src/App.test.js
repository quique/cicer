import React from "react";
import { render } from "@testing-library/react";
import App from "./App";

test("renders sign in", () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/Nombre de acceso/i);
  expect(linkElement).toBeInTheDocument();
});
