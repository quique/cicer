import React, { useState } from "react";
import { Form, Col } from "react-bootstrap";
import { Typeahead } from "react-bootstrap-typeahead";
import Fetcher from "../Fetcher";

function MemberPicker(props) {
  const [members, setStateMembers] = useState([]);
  const selected = props.member ? [props.member] : [];

  const setMembers = (members) => {
    const newMembers = members.map((m) => {
      m.numStr = m.num.toString();
      if (!m.login) {
        m.login = "";
      }
      return m;
    });
    setStateMembers(newMembers);

    if (!props.member && props.num) {
      const member = newMembers.find((m) => m.num === props.num);
      props.onChange(member);
    }
  };

  const text = props.text ? props.text : "Socia";

  return (
    <Fetcher url="/api/member" onFetch={setMembers}>
      <Form.Row>
        <Col sm={4}>
          <br />
          <h4 className="text-right">{text}:</h4>
        </Col>
        <Form.Group as={Col} sm={2}>
          <Form.Label>Num</Form.Label>
          <Typeahead
            id="member-num"
            labelKey="numStr"
            options={members}
            onChange={(m) => props.onChange(m[0])}
            selected={selected}
          />
        </Form.Group>
        <Form.Group as={Col}>
          <Form.Label>Login</Form.Label>
          <Typeahead
            id="member-login"
            labelKey="login"
            options={members}
            onChange={(m) => props.onChange(m[0])}
            selected={selected}
          />
        </Form.Group>
        <Form.Group as={Col}>
          <Form.Label>Nombre</Form.Label>
          <Typeahead
            id="member-name"
            labelKey="name"
            options={members}
            onChange={(m) => props.onChange(m[0])}
            selected={selected}
          />
        </Form.Group>
      </Form.Row>
      <hr />
    </Fetcher>
  );
}

export default MemberPicker;
