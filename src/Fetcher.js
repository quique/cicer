import React from "react";
import { Spinner, Alert, Row } from "react-bootstrap";
import AuthContext from "./AuthContext";
import { url } from "./util";

class Fetcher extends React.Component {
  static contextType = AuthContext;

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      error: null,
      url: null,
      refetch: null,
    };
  }

  componentDidMount() {
    this.fetch();
    if (!this.props.oneShot) {
      this.timerID = setInterval(
        () => this.fetch(),
        30000 // every 30 seconds
      );
    }
  }

  compomentWillUnmount() {
    clearInterval(this.timerID);
  }

  fetch() {
    if (this.state.error) {
      this.setState({ error: null });
    }
    fetch(url(this.props.url), {
      headers: { "x-authentication": this.context.token },
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error(
            response.status.toString() + " " + response.statusText
          );
        }
        return response.json();
      })
      .then((data) => {
        if (this.state.isLoading) {
          this.setState({ isLoading: false });
        }
        this.props.onFetch(data);
      })
      .catch((e) => this.setState({ error: e.message, isLoading: false }));
  }

  render() {
    // A hack to get it to fetch when url or refetch changes
    if (
      this.state.url !== this.props.url ||
      this.state.refetch !== this.props.refetch
    ) {
      this.setState({ url: this.props.url, refetch: this.props.refetch });
      this.fetch();
    }

    if (this.state.isLoading) {
      return (
        <Row className="justify-content-md-center">
          <Spinner animation="border" />
        </Row>
      );
    }

    if (this.state.error != null) {
      console.log(this.state.error);
      return (
        <Alert variant="danger">
          Ha ocurrido un error cargando datos: {this.state.error}
        </Alert>
      );
    }

    return this.props.children;
  }
}

export default Fetcher;
