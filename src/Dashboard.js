import React from "react";
import { Row, Col, Button } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import AuthContext from "./AuthContext";
import Fetcher from "./Fetcher";
import OrderCards from "./order/OrderCards";
import MyTransactions from "./transaction/MyTransactions";
import { printMoney } from "./util";

class Dashboard extends React.Component {
  static contextType = AuthContext;

  constructor(props) {
    super(props);
    this.state = {
      name: null,
      balance: null,
    };
  }

  render() {
    return (
      <Fetcher
        url={"/api/member/me"}
        onFetch={(member) =>
          this.setState({ balance: member.balance, name: member.name })
        }
      >
        <Row>
          <Col xs>
            <div className="text-right">
              <h6>{this.state.name}</h6>
              <h1>{printMoney(this.state.balance)}€</h1>
            </div>
          </Col>
          <Col xs={{ offset: 0 }} md={{ offset: 1 }}>
            <br />
            <LinkContainer to="/purchase">
              <Button variant="success">Comprar</Button>
            </LinkContainer>
          </Col>
        </Row>
        <OrderCards />
        <MyTransactions />
      </Fetcher>
    );
  }
}

export default Dashboard;
