import React from "react";
import { OverlayTrigger, Popover } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";

function transactionOverlay(transaction) {
  let title;
  let content;
  switch (transaction.type) {
    case "purchase":
      title = "compra";
      content =
        transaction.purchase
          .map((p) => (p.product !== null ? p.product.name : p.code))
          .join(",") + ".";
      break;
    case "topup":
      if (transaction.total < 0) {
        title = "devolución";
      } else {
        title = "recarga";
      }
      content = transaction.topup.comment;
      break;
    case "order":
      title = "pedido de " + transaction.order.name;
      content = transaction.order_purchase.map((p) => {
        const name =
          p.order_product.product !== null
            ? p.order_product.product.name
            : p.order_product.code;
        return (
          <div key={"O" + transaction.ID + "-" + p.order_product.ID}>
            {name + ": " + p.amount}
            <br />
          </div>
        );
      });
      break;
    case "refund":
      title = "devolución de " + transaction.refund.name;
      break;
    default:
      title = "transacción";
  }
  return (
    <Popover>
      <Popover.Title>{title}</Popover.Title>
      {content && <Popover.Content>{content}</Popover.Content>}
    </Popover>
  );
}

function TransactionTr(props) {
  const colorClass =
    props.transaction.total < 0 ? "table-danger" : "table-success";
  return (
    <OverlayTrigger
      overlay={transactionOverlay(props.transaction)}
      key={props.transaction.ID}
    >
      <LinkContainer to={"/transaction/" + props.transaction.ID}>
        <tr className={colorClass}>{props.children}</tr>
      </LinkContainer>
    </OverlayTrigger>
  );
}

export default TransactionTr;
