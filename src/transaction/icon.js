import React from "react";
import { FaShoppingBasket, FaMoneyBillAlt } from "react-icons/fa";
import { GiPayMoney, GiReceiveMoney } from "react-icons/gi";
import { HiClipboardCopy, HiClipboardList } from "react-icons/hi";

function icon(transaction) {
  switch (transaction.type) {
    case "purchase":
      return <FaShoppingBasket />;
    case "topup":
      if (transaction.total < 0) {
        return <GiReceiveMoney />;
      }
      return <GiPayMoney />;
    case "order":
      return <HiClipboardList />;
    case "refund":
      return <HiClipboardCopy />;
    default:
      return <FaMoneyBillAlt />;
  }
}

export default icon;
