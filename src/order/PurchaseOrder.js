import React from "react";
import { Alert, Form, InputGroup, Button, Col, Row } from "react-bootstrap";
import { printMoney } from "../util";
import Sender from "../Sender";

function calculateTotal(order) {
  const add = (acc, p) => acc + p.price * p.amount;
  return order.reduce(add, 0);
}

class PurchaseOrder extends React.Component {
  constructor(props) {
    super(props);

    const order = props.order.products
      .map((p) => {
        p.amount = 0;
        if (props.purchase) {
          const my_purchase = props.purchase.find(
            (e) => e.order_product.code === p.code
          );
          if (my_purchase) {
            p.amount = my_purchase.amount;
          }
        }
        return p;
      })
      .sort((p1, p2) => {
        if (p1.product.name > p2.product.name) {
          return 1;
        }
        if (p1.product.name < p2.product.name) {
          return -1;
        }
        return 0;
      });

    this.state = {
      order: order,
      total: calculateTotal(order),
      noMoney: false,
    };

    this.body = this.body.bind(this);
    this.onSuccess = this.onSuccess.bind(this);
    this.onError = this.onError.bind(this);
  }

  setAmount(index, amount) {
    let order = this.state.order;
    order[index].amount = amount;
    const total = calculateTotal(order);
    this.setState({ order, total });
  }

  body() {
    return this.state.order.map((p) => {
      return {
        order_product_id: p.ID,
        amount: parseInt(p.amount),
      };
    });
  }

  onSuccess(transaction) {
    this.props.onSend(transaction);
    this.setState({ noMoney: false });
  }

  onError(status) {
    if (status === 400) {
      this.setState({ noMoney: true });
      return true;
    }
    return false;
  }

  render() {
    let alert;
    if (this.state.noMoney) {
      alert = (
        <Alert variant="warning">
          No tienes suficiente dinero para realizar este pedido.
        </Alert>
      );
    }

    const formEntries = this.state.order.map((p, i) => (
      <Form.Group key={p.code} as={Row}>
        <Form.Label column className="text-right">
          {p.product.name} ({p.code}):
        </Form.Label>
        <Col>
          <InputGroup>
            <Form.Control
              type="number"
              min="0"
              placeholder="cantidad"
              value={p.amount}
              onChange={(e) => this.setAmount(i, e.target.value)}
            />
            <InputGroup.Append>
              <InputGroup.Text>{printMoney(p.price)}€</InputGroup.Text>
            </InputGroup.Append>
          </InputGroup>
        </Col>
      </Form.Group>
    ));

    return (
      <Sender
        url={"/api/order/" + this.props.order.ID + "/purchase"}
        body={this.body}
        onSuccess={this.onSuccess}
        onError={this.onError}
      >
        {alert}
        {formEntries}
        <Form.Group as={Row}>
          <Col className="text-right">
            <Button type="submit">
              {this.props.purchase ? "Actualizar" : "Realizar"} pedido
            </Button>
          </Col>
          <Col>
            <h3>Total: {printMoney(this.state.total)}€</h3>
          </Col>
        </Form.Group>
      </Sender>
    );
  }
}

export default PurchaseOrder;
