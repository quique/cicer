import React from "react";
import { Redirect } from "react-router-dom";
import { LinkContainer } from "react-router-bootstrap";
import Fetcher from "../Fetcher";
import {
  Row,
  Col,
  Badge,
  Button,
  ButtonGroup,
  Spinner,
  Alert,
  Modal,
} from "react-bootstrap";
import PurchaseOrder from "./PurchaseOrder";
import Sender from "../Sender";
import { printDate } from "../util";
import AuthContext from "../AuthContext";
import { printMoney, url } from "../util";

function getName(order_product) {
  if (order_product.product !== null) {
    return order_product.product.name;
  }
  return order_product.code;
}

function ShowOrderTransaction(props) {
  const list = props.transaction.order_purchase.map((o) => (
    <li key={o.order_product.code}>
      {getName(o.order_product)} ({o.order_product.code}): {o.amount}
    </li>
  ));
  return (
    <div>
      <h3>Mi pedido</h3>
      <ul>{list}</ul>
      <p>Total: {printMoney(-props.transaction.total)} €</p>
    </div>
  );
}

function ShowOrderResults(props) {
  let products = props.order.products.map((p) => {
    p.total = 0;
    return p;
  });
  const transactions = props.order.transactions.map((t) => {
    if (t.order_purchase === null) {
      return null;
    }
    const list = t.order_purchase.map((purchase) => {
      const i = products.findIndex((p) => p.ID === purchase.order_product_id);
      if (i === -1) {
        return null;
      }
      products[i].total += purchase.amount;
      if (purchase.amount) {
        const key =
          t.member.num.toString() + "-" + purchase.order_product_id.toString();
        return (
          <li key={key}>
            {getName(products[i])} {purchase.amount}
          </li>
        );
      }
      return null;
    });
    return (
      <li key={t.member.num}>
        {t.member.name} ({t.member.num}):
        {t.collected && <Badge variant="success">Recogido</Badge>}
        <ul>{list}</ul>
      </li>
    );
  });

  const product_list = products.map((p) => (
    <li key={p.code}>
      {getName(p)}: {p.total}
    </li>
  ));
  return (
    <div>
      <h3>Productos pedidos</h3>
      <ul>{product_list}</ul>

      <h3>Pedidos</h3>
      <ul>{transactions}</ul>
    </div>
  );
}

class ShowOrder extends React.Component {
  static contextType = AuthContext;

  constructor(props) {
    super(props);
    this.state = {
      order: {
        products: [],
        transactions: [],
      },
      transaction: null,
      isLoading: false,
      redirect: false,
      refetch: 0,
      error: null,
      showDelete: false,
    };
  }

  showTransaction() {
    if (this.state.order.active) {
      return (
        <PurchaseOrder
          order={this.state.order}
          purchase={
            this.state.transaction && this.state.transaction.order_purchase
          }
          onSend={(t) => this.onSend(t)}
        />
      );
    }
    if (this.state.transaction) {
      return (
        <ShowOrderTransaction
          order={this.state.order}
          transaction={this.state.transaction}
        />
      );
    }
  }

  onSend(transaction) {
    this.setState({ transaction, refetch: this.state.refetch + 1 });
  }

  setData(data) {
    this.setState({ order: data.order, transaction: data.transaction });
  }

  delorder() {
    this.setState({ isLoading: true });
    fetch(url("/api/order/" + this.state.order.ID), {
      method: "DELETE",
      headers: { "x-authentication": this.context.token },
    }).then((response) => {
      if (!response.ok) {
        this.setState({
          error:
            "Ha ocurrido un error cancelando el pedido: " +
            response.status.toString() +
            " " +
            response.statusText,
          isLoading: false,
        });
      } else {
        this.setState({ redirect: true });
      }
    });
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to="/" />;
    }

    const order = this.state.order;
    let expired;
    if (!order.active) {
      expired = <Badge variant="info">finalizado</Badge>;
    }

    const { id } = this.props.match.params;

    let update_button;
    let collect_button;
    if (this.state.isLoading) {
      update_button = <Spinner animation="border" />;
    } else {
      let deadline_week = new Date(order.deadline);
      deadline_week.setDate(deadline_week.getDate() + 7);
      if (deadline_week > Date.now()) {
        if (
          order.arrived &&
          this.state.transaction !== null &&
          !this.state.transaction.collected
        ) {
          collect_button = (
            <Sender
              url={"/api/order/" + id + "/collected"}
              method="PUT"
              onSuccess={() =>
                this.setState({ refetch: this.state.refetch + 1 })
              }
            >
              <Button type="submit" variant="secondary">
                Recogido
              </Button>
            </Sender>
          );
        }
        if (
          order.member_num === parseInt(this.context.num) ||
          this.context.role === "admin"
        ) {
          let arrived_button;
          if (!order.active && !order.arrived) {
            arrived_button = (
              <Sender
                url={"/api/order/" + id + "/arrive"}
                method="PUT"
                onSuccess={() =>
                  this.setState({ refetch: this.state.refetch + 1 })
                }
              >
                <Button type="submit" variant="dark">
                  Informar llegada
                </Button>
              </Sender>
            );
          }
          update_button = (
            <div>
              <ButtonGroup>
                <LinkContainer to={"/order/edit/" + id}>
                  <Button variant="info">Modificar</Button>
                </LinkContainer>
                <Button
                  variant="danger"
                  onClick={() => this.setState({ showDelete: true })}
                >
                  Cancelar
                </Button>
              </ButtonGroup>
              <br />
              {arrived_button}
            </div>
          );
        }
      }
    }

    const deadlineStr = printDate(this.state.order.deadline, true);

    const handleClose = () => this.setState({ showDelete: false });

    return (
      <Fetcher
        url={"/api/order/" + id}
        onFetch={(data) => this.setData(data)}
        refetch={this.state.refetch}
      >
        {this.state.error && <Alert variant="danger">{this.state.error}</Alert>}
        <Row>
          <Col>
            <h1>{this.state.order.name}</h1>
          </Col>
          <Col className="text-right">
            <p>
              Fecha limite:
              <br />
              {deadlineStr}
              <br />
              {expired}
            </p>
            {update_button}
            {collect_button}
          </Col>
        </Row>
        <p>{this.state.order.description}</p>

        {this.showTransaction()}
        <ShowOrderResults order={this.state.order} />

        <Modal show={this.state.showDelete} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Confirmar la elminicacion</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <p>¿Borrar permanentemente el pedido {this.state.order.name}?</p>
            <p>Si aceptas devolvera el dinero a quien haya pedido productos.</p>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Cancelar
            </Button>
            <Button variant="danger" onClick={() => this.delorder()}>
              Eliminar
            </Button>
          </Modal.Footer>
        </Modal>
      </Fetcher>
    );
  }
}

export default ShowOrder;
