import React, { useState } from "react";
import { Form } from "react-bootstrap";
import { printMoney } from "./util";
import PriceEditor from "./PriceEditor";

function EditableCell(props) {
  const [data, setData] = useState(props.value);
  const [editing, setEditing] = useState(false);

  let value = props.value;
  if (props.price) {
    value = printMoney(props.value);
  }
  if (props.ro || !editing) {
    return <td onClick={() => setEditing(true)}>{value}</td>;
  }

  const submit = (e) => {
    e.preventDefault();
    props.onChange(data);
    setEditing(false);
  };

  return (
    <td>
      <Form onSubmit={submit}>
        {props.price ? (
          <PriceEditor value={data} onChange={(price) => setData(price)} />
        ) : (
          <Form.Control
            value={data}
            onChange={(e) => setData(e.target.value)}
            onBlur={() => setEditing(false)}
          />
        )}
      </Form>
    </td>
  );
}

export default EditableCell;
