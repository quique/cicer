import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { Container, Row } from "react-bootstrap";
import MemberAdder from "./member/MemberAdder";
import MemberEditer from "./member/MemberEditer";
import MemberList from "./member/MemberList";
import ProductList from "./product/ProductList";
import AnnualReport from "./product/AnnualReport";
import ShowProduct from "./product/ShowProduct";
import CreateProduct from "./product/CreateProduct";
import Inventary from "./inventary/Inventary";
import ShowInventary from "./inventary/ShowInventary";
import CreateSupplier from "./inventary/CreateSupplier";
import Dashboard from "./Dashboard";
import OwnPassword from "./OwnPassword";
import PurchaseList from "./purchase/PurchaseList";
import Purchase from "./purchase/Purchase";
import Topup from "./Topup";
import ShowTransaction from "./transaction/ShowTransaction";
import TransactionList from "./transaction/TransactionList";
import ShowOrder from "./order/ShowOrder";
import CreateOrder from "./order/CreateOrder";
import OrderList from "./order/OrderList";
import EditOrder from "./order/EditOrder";
import SignIn from "./SignIn";
import ResetRequest from "./ResetRequest";
import ResetPassword from "./ResetPassword";
import Head from "./Head";
import logo from "./logo.svg";

function Panel(props) {
  if (props.isLogged) {
    return <LogedPanel onLogout={props.onLogout} />;
  }
  return <UnlogedPanel onLogin={props.onLogin} />;
}

function LogedPanel(props) {
  return (
    <div>
      <BrowserRouter>
        <Head onLogout={props.onLogout} />
        <Container>
          <Switch>
            <Route path="/members/add">
              <MemberAdder />
            </Route>
            <Route path="/member/:num">
              <MemberEditer />
            </Route>
            <Route path="/members/purchase">
              <Purchase member />
            </Route>
            <Route path="/members">
              <MemberList />
            </Route>
            <Route path="/products">
              <ProductList />
            </Route>
            <Route path="/annual">
              <AnnualReport />
            </Route>
            <Route path="/product/add">
              <CreateProduct />
            </Route>
            <Route path="/product/:code">
              <ShowProduct />
            </Route>
            <Route path="/inventary/:id">
              <ShowInventary />
            </Route>
            <Route path="/inventary">
              <Inventary />
            </Route>
            <Route path="/supplier/add">
              <CreateSupplier />
            </Route>
            <Route path="/transaction/:id">
              <ShowTransaction />
            </Route>
            <Route path="/transaction">
              <TransactionList />
            </Route>
            <Route path="/password">
              <OwnPassword />
            </Route>
            <Route path="/purchase">
              <Purchase />
            </Route>
            <Route path="/purchases">
              <PurchaseList />
            </Route>
            <Route path="/topup/:num">
              <Topup />
            </Route>
            <Route path="/topup">
              <Topup />
            </Route>
            <Route path="/orders">
              <OrderList />
            </Route>
            <Route path="/order/create">
              <CreateOrder />
            </Route>
            <Route path="/order/edit/:id" component={EditOrder} />
            <Route path="/order/:id" component={ShowOrder} />
            <Route path="/">
              <Dashboard />
            </Route>
          </Switch>
        </Container>
      </BrowserRouter>
    </div>
  );
}

function UnlogedPanel(props) {
  return (
    <Container>
      <Row className="justify-content-center">
        <img src={logo} alt="Garbanzo Negro" />
      </Row>
      <BrowserRouter>
        <Switch>
          <Route path="/reset/:token" component={ResetPassword} />
          <Route path="/reset/" component={ResetRequest} />
          <Route path="/">
            <SignIn onLogin={props.onLogin} />
          </Route>
        </Switch>
      </BrowserRouter>
    </Container>
  );
}

export default Panel;
