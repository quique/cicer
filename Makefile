.PHONY: build test apk

build:
	go build
	npm install
	npm run build

test:
	go test ./...
	npm test

apk:
	REACT_APP_API="https://cicer.garbanzonegro.org" REACT_APP_NATIVE="true" npm run build
	npx cap copy
	cd android && ./gradlew assembleRelease
