module 0xacab.org/meskio/cicer

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/mux v1.8.0
	github.com/jstemmer/gotags v1.4.1 // indirect
	github.com/olivere/env v1.1.0
	golang.org/x/crypto v0.0.0-20190308221718-c2843e01d9a2
	gorm.io/driver/sqlite v1.1.2
	gorm.io/gorm v1.20.1
)
