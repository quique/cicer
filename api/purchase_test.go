package api

import (
	"fmt"
	"net/http"
	"testing"

	"0xacab.org/meskio/cicer/api/db"
)

func TestPurchaseAddListMine(t *testing.T) {
	tapi := newTestAPI(t)
	defer tapi.close()
	tapi.addTestMember()
	tapi.addTestProducts()

	products := []db.Purchase{
		{
			ProductCode: testProduct.Code,
			Amount:      5,
		},
	}
	resp := tapi.do("POST", "/purchase", products, nil)
	if resp.StatusCode != http.StatusCreated {
		t.Fatal("Can't create purchase:", resp.Status)
	}
	var transactions []db.Transaction
	resp = tapi.do("GET", "/transaction/mine", nil, &transactions)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get transactions:", resp.Status)
	}

	if len(transactions) != 1 {
		t.Fatal("Wrong number of transactions", len(transactions), transactions)
	}
	if transactions[0].Total != -testProduct.Price*products[0].Amount {
		t.Error("Wrong total:", transactions[0].Total)
	}
	if len(transactions[0].Purchase) != 1 {
		t.Fatal("Wrong number of products", len(transactions[0].Purchase), transactions[0].Purchase)
	}
	if transactions[0].Purchase[0].ProductCode != testProduct.Code {
		t.Error("Wrong product code:", transactions[0].Purchase[0].ProductCode)
	}
	if transactions[0].Purchase[0].Price != testProduct.Price {
		t.Error("Wrong product price:", transactions[0].Purchase[0].Price)
	}

	var product db.Product
	resp = tapi.do("GET", "/product/234", nil, &product)
	if resp.StatusCode != http.StatusOK {
		t.Error("Can't find the product:", resp.Status)
	}
	if product.Stock != testProduct.Stock-products[0].Amount {
		t.Error("Wrong product stock:", product)
	}

	var member db.Member
	resp = tapi.do("GET", "/member/me", nil, &member)
	if resp.StatusCode != http.StatusOK {
		t.Error("Can't find the member:", resp.Status)
	}
	if member.Balance != testMember.Balance-(testProduct.Price*products[0].Amount) {
		t.Error("Wrong product balance:", member.Balance)
	}
}

func TestMemberPurchase(t *testing.T) {
	tapi := newTestAPI(t)
	defer tapi.close()
	tapi.addTestMember()
	tapi.addTestProducts()

	products := []db.Purchase{
		{
			ProductCode: testProduct.Code,
			Amount:      5,
		},
	}
	resp := tapi.doAdmin("POST", fmt.Sprintf("/member/%d/purchase", testMember.Num), products, nil)
	if resp.StatusCode != http.StatusCreated {
		t.Fatal("Can't create purchase:", resp.Status)
	}
	var transactions []db.Transaction
	resp = tapi.do("GET", "/transaction/mine", nil, &transactions)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get transactions:", resp.Status)
	}

	if len(transactions) != 1 {
		t.Fatal("Wrong number of transactions", len(transactions), transactions)
	}
	if transactions[0].Proxy.Num != testMemberAdmin.Num {
		t.Error("Wrong proxy:", transactions[0].Proxy.Num)
	}
	if transactions[0].Total != -testProduct.Price*products[0].Amount {
		t.Error("Wrong total:", transactions[0].Total)
	}
	if len(transactions[0].Purchase) != 1 {
		t.Fatal("Wrong number of products", len(transactions[0].Purchase), transactions[0].Purchase)
	}
	if transactions[0].Purchase[0].ProductCode != testProduct.Code {
		t.Error("Wrong product code:", transactions[0].Purchase[0].ProductCode)
	}
	if transactions[0].Purchase[0].Price != testProduct.Price {
		t.Error("Wrong product price:", transactions[0].Purchase[0].Price)
	}

	var product db.Product
	resp = tapi.do("GET", "/product/234", nil, &product)
	if resp.StatusCode != http.StatusOK {
		t.Error("Can't find the product:", resp.Status)
	}
	if product.Stock != testProduct.Stock-products[0].Amount {
		t.Error("Wrong product stock:", product)
	}

	var member db.Member
	resp = tapi.do("GET", "/member/me", nil, &member)
	if resp.StatusCode != http.StatusOK {
		t.Error("Can't find the member:", resp.Status)
	}
	if member.Balance != testMember.Balance-(testProduct.Price*products[0].Amount) {
		t.Error("Wrong product balance:", member.Balance)
	}
}
