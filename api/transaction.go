package api

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"strconv"

	"0xacab.org/meskio/cicer/api/db"
	"github.com/gorilla/mux"
)

func (a *api) ListTransactions(w http.ResponseWriter, req *http.Request) {
	a.getTransactionsByMember(0, w, req)
}

func (a *api) GetTransaction(num int, role string, w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	id, _ := strconv.Atoi(vars["id"])
	transaction, err := a.db.GetTransaction(id)
	if err != nil {
		if errors.Is(err, db.ErrorNotFound) {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		log.Printf("Can't get transaction %s: %v", vars["id"], err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if transaction.MemberNum != num && role != "admin" {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(transaction)
	if err != nil {
		log.Printf("Can't encode transaction: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (a *api) GetMemberTransactions(w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	num, _ := strconv.Atoi(vars["num"])
	a.getTransactionsByMember(num, w, req)
}

func (a *api) getTransactionsByMember(num int, w http.ResponseWriter, req *http.Request) {
	err := req.ParseForm()
	if err != nil {
		log.Printf("Can't parse the query: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	transactions, err := a.db.ListTransactions(num, req.Form)
	if err != nil {
		log.Printf("Can't list transactions: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(transactions)
	if err != nil {
		log.Printf("Can't encode transactions: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}
