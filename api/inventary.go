package api

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"0xacab.org/meskio/cicer/api/db"
	"github.com/gorilla/mux"
)

func (a *api) AddSupplier(w http.ResponseWriter, req *http.Request) {
	var supplier db.Supplier
	err := json.NewDecoder(req.Body).Decode(&supplier)
	if err != nil {
		log.Printf("Can't decode supplier: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	err = a.db.AddSupplier(&supplier)
	if err != nil {
		log.Printf("Can't create supplier: %v\n%v", err, supplier)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	err = json.NewEncoder(w).Encode(supplier)
	if err != nil {
		log.Printf("Can't encode added supplier: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (a *api) ListSuppliers(w http.ResponseWriter, req *http.Request) {
	suppliers, err := a.db.ListSuppliers()
	if err != nil {
		log.Printf("Can't list suppliers: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(suppliers)
	if err != nil {
		log.Printf("Can't encode suppliers: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (a *api) AddInventary(num int, w http.ResponseWriter, req *http.Request) {
	var inventary db.Inventary
	err := json.NewDecoder(req.Body).Decode(&inventary)
	if err != nil {
		log.Printf("Can't decode inventary: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	err = a.db.AddInventary(num, &inventary)
	if err != nil {
		log.Printf("Can't create inventary: %v\n%v", err, inventary)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	err = json.NewEncoder(w).Encode(inventary)
	if err != nil {
		log.Printf("Can't encode added inventary: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (a *api) GetInventary(w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	id, _ := strconv.Atoi(vars["id"])
	inventary, err := a.db.GetInventary(id)
	if err != nil {
		log.Printf("Can't get inventary: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(inventary)
	if err != nil {
		log.Printf("Can't encode inventary: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (a *api) ListInventary(w http.ResponseWriter, req *http.Request) {
	inventary, err := a.db.ListInventary()
	if err != nil {
		log.Printf("Can't list inventary: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(inventary)
	if err != nil {
		log.Printf("Can't encode inventary: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}
