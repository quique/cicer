package db

import (
	"errors"
)

var (
	ErrorBadPassword    = errors.New("Bad password")
	ErrorInvalidRequest = errors.New("Invalid request")
	ErrorNotFound       = errors.New("Record not found")
)
