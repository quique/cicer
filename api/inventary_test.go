package api

import (
	"fmt"
	"net/http"
	"testing"

	"0xacab.org/meskio/cicer/api/db"
)

var testSupplier = db.Supplier{
	Name: "Aceites Geronimo",
}

func TestSupplierAddList(t *testing.T) {
	tapi := newTestAPI(t)
	defer tapi.close()
	tapi.addTestSuppliers()

	var suppliers []db.Supplier
	resp := tapi.do("GET", "/supplier", nil, &suppliers)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get suppliers:", resp.Status)
	}

	if len(suppliers) != 1 {
		t.Fatal("Wrong number of suppliers", len(suppliers), suppliers)
	}
	if suppliers[0].Name != testSupplier.Name {
		t.Error("Wrong name:", suppliers[0].Name)
	}
}

func TestInventaryUpdateStock(t *testing.T) {
	tapi := newTestAPI(t)
	defer tapi.close()
	supplierID := tapi.addTestSuppliers()
	tapi.addTestProducts()

	update := -5
	inventary := db.Inventary{
		SupplierID: &supplierID,
		Comment:    "Got aceite",
		Products: []db.InventaryProduct{
			{
				ProductCode: testProduct.Code,
				StockUpdate: &update,
			},
		},
	}
	resp := tapi.doAdmin("POST", "/inventary", inventary, nil)
	if resp.StatusCode != http.StatusCreated {
		t.Fatal("Can't create inventary:", resp.Status)
	}

	var newInventary []db.Inventary
	resp = tapi.doAdmin("GET", "/inventary", nil, &newInventary)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get inventary:", resp.Status)
	}
	if len(newInventary) != 1 {
		t.Fatal("Wrong inventary length:", newInventary)
	}
	if newInventary[0].Comment != inventary.Comment {
		t.Error("Wrong comment:", newInventary[0].Comment)
	}
	if len(newInventary[0].Products) != 1 {
		t.Error("Wrong products:", newInventary[0].Products)
	}

	var entry db.Inventary
	resp = tapi.doAdmin("GET", fmt.Sprintf("/inventary/%d", newInventary[0].ID), nil, &entry)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get inventary:", resp.Status)
	}
	if entry.Comment != inventary.Comment {
		t.Error("Wrong comment:", entry.Comment)
	}
	if len(entry.Products) != 1 {
		t.Error("Wrong products:", entry.Products)
	}

	var product db.Product
	resp = tapi.doAdmin("GET", fmt.Sprintf("/product/%d", testProduct.Code), nil, &product)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get product:", resp.Status)
	}
	if product.Stock != testProduct.Stock+*inventary.Products[0].StockUpdate {
		t.Error("Wrong stock:", product.Stock)
	}
}

func TestInventaryUpdatePrice(t *testing.T) {
	tapi := newTestAPI(t)
	defer tapi.close()
	supplierID := tapi.addTestSuppliers()
	tapi.addTestProducts()

	update := 1000
	inventary := db.Inventary{
		SupplierID: &supplierID,
		Comment:    "Got aceite",
		Products: []db.InventaryProduct{
			{
				ProductCode: testProduct.Code,
				Price:       &update,
			},
		},
	}
	resp := tapi.doAdmin("POST", "/inventary", inventary, nil)
	if resp.StatusCode != http.StatusCreated {
		t.Fatal("Can't create inventary:", resp.Status)
	}

	var product db.Product
	resp = tapi.doAdmin("GET", fmt.Sprintf("/product/%d", testProduct.Code), nil, &product)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get product:", resp.Status)
	}
	if product.Price != *inventary.Products[0].Price {
		t.Error("Wrong price:", product.Price)
	}
}

func TestInventaryUpdateBoth(t *testing.T) {
	tapi := newTestAPI(t)
	defer tapi.close()
	supplierID := tapi.addTestSuppliers()
	tapi.addTestProducts()

	updateStock := 5
	price := 800
	inventary := db.Inventary{
		SupplierID: &supplierID,
		Comment:    "Got aceite",
		Products: []db.InventaryProduct{
			{
				ProductCode: testProduct.Code,
				StockUpdate: &updateStock,
				Price:       &price,
			},
		},
	}
	resp := tapi.doAdmin("POST", "/inventary", inventary, nil)
	if resp.StatusCode != http.StatusCreated {
		t.Fatal("Can't create inventary:", resp.Status)
	}

	var product db.Product
	resp = tapi.doAdmin("GET", fmt.Sprintf("/product/%d", testProduct.Code), nil, &product)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get product:", resp.Status)
	}
	if product.Stock != testProduct.Stock+*inventary.Products[0].StockUpdate {
		t.Error("Wrong stock:", product.Stock)
	}
	newPrice := ((price * updateStock) + (testProduct.Price * testProduct.Stock)) / (updateStock + testProduct.Stock)
	if product.Price != newPrice {
		t.Error("Wrong price:", product.Price)
	}
}

func (tapi *testAPI) addTestSuppliers() uint {
	var supplier db.Supplier
	resp := tapi.doAdmin("POST", "/supplier", testSupplier, &supplier)
	if resp.StatusCode != http.StatusCreated {
		tapi.t.Fatal("Can't create supplier:", resp.Status)
	}
	return supplier.ID
}
