package api

import (
	"net/http"
	"testing"

	"0xacab.org/meskio/cicer/api/db"
)

var testProduct = db.Product{
	Code:  234,
	Name:  "Aceite",
	Price: 1700,
	Stock: 10,
}
var testProduct2 = db.Product{
	Code:  123,
	Name:  "Huevos",
	Price: 120,
	Stock: 15,
}

func TestProductAddList(t *testing.T) {
	tapi := newTestAPI(t)
	defer tapi.close()
	tapi.addTestProducts()

	var products []db.Product
	resp := tapi.do("GET", "/product", nil, &products)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get products:", resp.Status)
	}

	for _, product := range products {
		if product.Name == testProduct.Name {
			if product.Price != testProduct.Price {
				t.Error("Wrong price:", products[0].Price)
			}
			return
		}
	}

	t.Error("Can't find testProduct in product list:", products)
}

func TestProductGetDelete(t *testing.T) {
	tapi := newTestAPI(t)
	defer tapi.close()

	resp := tapi.do("GET", "/product/234", nil, nil)
	if resp.StatusCode != http.StatusNotFound {
		t.Error("Expected not found:", resp.Status, resp.Body)
	}
	tapi.addTestProducts()

	var gotProduct db.Product
	resp = tapi.do("GET", "/product/234", nil, &gotProduct)
	if resp.StatusCode != http.StatusOK {
		t.Error("Can't find the product:", resp.Status)
	}
	if gotProduct.Code != testProduct.Code {
		t.Error("Wrong product:", gotProduct.Code)
	}
	resp = tapi.doAdmin("DELETE", "/product/234", nil, nil)
	if resp.StatusCode != http.StatusOK {
		t.Error("Can't find the product:", resp.Status)
	}

	resp = tapi.do("GET", "/product/234", nil, nil)
	if resp.StatusCode != http.StatusNotFound {
		t.Error("Expected not found after delete:", resp.Status, resp.Body)
	}
}

func TestProductUpdate(t *testing.T) {
	tapi := newTestAPI(t)
	defer tapi.close()
	tapi.addTestProducts()

	product := testProduct
	product.Stock = testProduct.Stock - 5
	resp := tapi.doAdmin("PUT", "/product/234", product, nil)
	if resp.StatusCode != http.StatusAccepted {
		t.Fatal("Can't update product:", resp.Status)
	}

	var gotProduct db.Product
	resp = tapi.do("GET", "/product/234", nil, &gotProduct)
	if resp.StatusCode != http.StatusOK {
		t.Error("Can't find the product:", resp.Status)
	}
	if gotProduct.Stock != 5 {
		t.Error("Wrong sotck:", gotProduct)
	}
}

func (tapi *testAPI) addTestProducts() {
	resp := tapi.doAdmin("POST", "/product", testProduct, nil)
	if resp.StatusCode != http.StatusCreated {
		tapi.t.Fatal("Can't create product:", resp.Status)
	}

	resp = tapi.doAdmin("POST", "/product", testProduct2, nil)
	if resp.StatusCode != http.StatusCreated {
		tapi.t.Fatal("Can't create product:", resp.Status)
	}
}
