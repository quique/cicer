package api

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"strconv"

	"0xacab.org/meskio/cicer/api/db"
	"github.com/gorilla/mux"
)

func (a *api) AddMember(w http.ResponseWriter, req *http.Request) {
	var memberReq db.MemberReq
	err := json.NewDecoder(req.Body).Decode(&memberReq)
	if err != nil {
		log.Printf("Can't create member: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	member, err := a.db.AddMember(&memberReq)
	if err != nil {
		log.Printf("Can't create member: %v\n%v", err, member)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if memberReq.Password == "" {
		_, token, err := a.db.NewPasswordReset(memberReq.Email)
		if err != nil {
			log.Printf("Can't create the password reset for %s: %v", memberReq.Email, err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		err = a.mail.sendNewMember(memberReq.Member, "/reset/"+token)
		if err != nil {
			log.Printf("Can't send the pass/login reset email to %s: %v", memberReq.Email, err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	err = json.NewEncoder(w).Encode(member)
	if err != nil {
		log.Printf("Can't encode added member: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (a *api) ListMembers(w http.ResponseWriter, req *http.Request) {
	members, err := a.db.ListMembers()
	if err != nil {
		log.Printf("Can't list members: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(members)
	if err != nil {
		log.Printf("Can't encode members: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (a *api) GetMember(w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	num, _ := strconv.Atoi(vars["num"])
	a.getMemberNum(num, w, req)
}

func (a *api) getMemberNum(num int, w http.ResponseWriter, req *http.Request) {
	member, err := a.db.GetMember(num)
	if err != nil {
		if errors.Is(err, db.ErrorNotFound) {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		log.Printf("Can't get member %d: %v", num, err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(member)
	if err != nil {
		log.Printf("Can't encode member: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (a *api) DeleteMember(w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	num, _ := strconv.Atoi(vars["num"])
	err := a.db.DeleteMember(num)
	if err != nil {
		log.Printf("Can't delete member %s: %v", vars["num"], err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func (a *api) UpdateMember(w http.ResponseWriter, req *http.Request) {
	var member db.MemberReq
	err := json.NewDecoder(req.Body).Decode(&member)
	if err != nil {
		log.Printf("Can't decode member: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	vars := mux.Vars(req)
	num, err := strconv.Atoi(vars["num"])
	if err != nil {
		log.Printf("Invalid member num %s: %v", vars["num"], err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	m, err := a.db.UpdateMember(num, member, false)
	if err != nil {
		if errors.Is(err, db.ErrorNotFound) {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		log.Printf("Can't update member %d: %v", num, err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusAccepted)
	err = json.NewEncoder(w).Encode(m)
	if err != nil {
		log.Printf("Can't encode updated member: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (a *api) UpdateMemberMe(num int, w http.ResponseWriter, req *http.Request) {
	var member db.MemberReq
	err := json.NewDecoder(req.Body).Decode(&member)
	if err != nil {
		log.Printf("Can't decode member: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	member.Num = 0
	member.Role = ""
	m, err := a.db.UpdateMember(num, member, true)
	if err != nil {
		if errors.Is(err, db.ErrorNotFound) {
			w.WriteHeader(http.StatusNotFound)
		} else if errors.Is(err, db.ErrorBadPassword) {
			w.WriteHeader(http.StatusBadRequest)
		} else {
			log.Printf("Can't update member %d: %v", num, err)
			w.WriteHeader(http.StatusInternalServerError)
		}
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusAccepted)
	err = json.NewEncoder(w).Encode(m)
	if err != nil {
		log.Printf("Can't encode updated member: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}
